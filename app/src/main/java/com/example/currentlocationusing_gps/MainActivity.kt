package com.example.currentlocationusing_gps

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AbsSeekBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity(),LocationListener {
    lateinit var locationManager: LocationManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getLocation()
    }
    private fun getLocation(){
        locationManager= getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) !=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    ,2)

        }
         locationManager.requestLocationUpdates(
        LocationManager.GPS_PROVIDER,0,5f,this)
    }

    override fun onLocationChanged(location: Location) {
        permissionStatusTextView.text= "latitude "+location.latitude+"longitude"+location.longitude

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode==2){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
              Toast.makeText(this, "Permission Granted!!!! location show in Toast",Toast.LENGTH_LONG
              ).show()            }
            else{
                Toast.makeText(this, "Permission Denied",Toast.LENGTH_LONG
                ).show()
            }
        }

    }
}